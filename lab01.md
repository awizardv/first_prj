#    Ansible                    Chef                            SaltStack                   Puppet                          Terraform
* PUSH-модель                PULL-модель                     PUSH/PULL-модель           PULL-модель                     PUSH-модель     
* Написан на Python          Написан на RUBY                 Написан на Python          Написан на RUBY                 Написан на GO
* YAML-подобный DSL          RUBY-подобный DSL               YAML-подобный DSL          Puppet DSL                      HCL / JSON
* Процедурный язык           Процедурный язык                Декларативный язык         Декларативный язык              Декларативный язык
* Низкий порог входа         Требует знания Ruby             Требует знания python      Требует знания Puppet DSL       Поддерживает любой язык
* Не подходит для DMZ        Подходит для DMZ                Подходит для DMZ           Подходит для DMZ                Решения для облаков

# Chef
* PULL-модель
* Написан на RUBY
* RUBY-подобный DSL
* Процедурный язык
* Требует знания Ruby 
* Подходит для DMZ

# SaltStack
* PUSH/PULL-модель
* Написан на Python
* YAML-подобный DSL
* Декларативный язык
* Требует знания python
* Подходит для DMZ

# Puppet
* PULL-модель 
* Написан на RUBY
* Puppet DSL 
* Декларативный язык
* Требует знания Puppet DSL
* Подходит для DMZ

# Terraform
* PUSH-модель
* Написан на GO
* HCL / JSON
* Декларативный язык
* Поддерживает любой язык
* Решения для облаков
